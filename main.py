print("hello world")

from fuzzywuzzy import fuzz

ex1 = fuzz.ratio("bonjour","bonsoir")

ex2 =fuzz.ratio("cours de conception logicielle", "portabilite")

print(ex1)

print(ex2)

import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def root():
    return {"result": "ok"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)

